# Python image to use.
FROM python:3.7
# Set the working directory to /app
WORKDIR /app
# copy the requirements file used for dependencies
COPY requirements.txt .
# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt
#RUN pip install ptvsd -- never run it on production ;) 
COPY ./src /app
# should be 8080...  it's such typical I know .. 
EXPOSE 8082 
ENTRYPOINT ["python"]
CMD ["/app/server.py"]