connexion
waitress==1.3.0
#git+https://github.com/lumikanta/strict-rfc3339.git@feature/ascii-only-regex#egg=strict-rfc3339
python-dateutil==2.8.0
gevent==1.4.0
gunicorn==19.9.0
werkzeug==0.16.1