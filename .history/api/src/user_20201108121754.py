import os,sys
from datetime import datetime
import time
import json
import connexion
import base64
from uuid import uuid4

def create(body):
  # let's create time for  request 
  servertime=str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
  userAgent=connexion.request.headers.get("User-Agent",None)
  
  # saving not duplicate request for example...  database  
  apiuuid=uuid4().hex
  newbody=body.copy()
  name=body.get("name","") 
  email=body.get("email","")
  age=body.get("age","")
  
  #save to database 
  #placeholder 

  try:
    return {"status":200,"name":name,"email":email,"age":age},200
  except Exception as error:
    return {"status":500,"servertime":servertime,"detail":"{}: {}".format(sys.exc_info()[2].tb_lineno, error)},500
