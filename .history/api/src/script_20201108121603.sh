#!/bin/bash

SRC_DIR=$1 #$(System.DefaultWorkingDirectory)/$(apiName)/api
API_PREFIX=$2 #$(apiPrefix)
CLOUD=$3 #azure/gcp
RES_WORKER=$4 # Number of worker in Dockerfile
RES_THREAD=$5 # Number of Thread in Dockerfile

cd $SRC_DIR
cd ..

sed -i "s/{var:resWorker}/$RES_WORKER/g" Dockerfile   
sed -i "s/{var:resThread}/$RES_THREAD/g" Dockerfile

find $SRC_DIR
