#!/bin/bash

ENV=$1 #$(env)
API_DIR=$2 #$(System.DefaultWorkingDirectory)/$(apiName)
API_PREFIX=$3 #$(apiPrefix)
BUILD_DIR=$4 #$(build.ArtifactStagingDirectory)
if [ "$5" != "-" ]
then
    BUILD_VER=":$5" #$(Build.SourceVersion) - build ID stays empty when "-" is provided
fi
CLOUD=$6 #azure/gcp
REGISTRY=$7
REPLICA=$8
CPU_L=$9
MEMORY_L=${10}
CPU_R=${11}
MEMORY_R=${12}

CPU_LIMIT_SEC=${13} # setting limits to esp containers 
CPU_REQUEST_SEC=${14}

echo "ENV=$ENV"
echo "API_DIR=$API_DIR"
echo "SRC_DIR=$SRC_DIR"
echo "API_PREFIX=$API_PREFIX"
echo "BUILD_DIR=$BUILD_DIR"
echo "BUILD_VER=$BUILD_VER"
echo "CLOUD=$CLOUD"
echo "REGISTRY=$REGISTRY"
echo "REPLICA=$REPLICA"
echo "CPU_L=$CPU_L"
echo "MEMORY_L=$MEMORY_L"
echo "CPU_R=$CPU_R"
echo "MEMORY_R=$MEMORY_R"
echo "CPU_LIMIT_SEC=$CPU_LIMIT_SEC"
echo "CPU_REQUEST_SEC=$CPU_REQUEST_SEC"

cp $API_DIR/kubernetes-manifests/$CLOUD-deployment.yaml $BUILD_DIR/deployment.yaml
cp $API_DIR/kubernetes-manifests/$CLOUD-service.yaml $BUILD_DIR/deployment-service.yaml

sed -i "s/{var:build}/$BUILD_VER/g" $BUILD_DIR/deployment.yaml
#sed -i "s|{var:reg}|$REGISTRY|g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:env}/$ENV/g" $BUILD_DIR/deployment.yaml

sed -i "s/{var:replica}/$REPLICA/g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:cpulimit}/$CPU_L/g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:memorylimit}/$MEMORY_L/g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:cpurequest}/$CPU_R/g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:memoryrequest}/$MEMORY_R/g" $BUILD_DIR/deployment.yaml

sed -i "s/{var:cpulimitsecond}/$CPU_LIMIT_SEC/g" $BUILD_DIR/deployment.yaml
sed -i "s/{var:cpurequestsecond}/$CPU_REQUEST_SEC/g" $BUILD_DIR/deployment.yaml
