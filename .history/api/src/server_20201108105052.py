from flask import render_template,request
from waitress import serve
import connexion
import json
import os
import re
import logging

# Instantiates a client
logging.info("start server")

specification_dir='./specification/'
specification_file='app-swagger.yaml'

# Create the application instance
app = connexion.App(__name__, specification_dir=specification_dir
    , debug=False, options={"swagger_ui": False})
app.add_api(specification_file)

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html')

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080=)
