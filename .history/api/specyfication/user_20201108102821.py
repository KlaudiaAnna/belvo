import os,sys
from google.cloud import pubsub_v1
from datetime import datetime
import time
import json
import connexion
import base64
import hashlib
#from safetynet_attestation import Attestation
from uuid import uuid4

os.environ["TZ"] = "UTC"
time.tzset()

def checknonce(nonce,value):
    result=False
    # try first binary base64
    checksum=base64.b64encode(hashlib.sha256(bytes(value,"UTF-8")).digest()).decode('UTF-8')
    if (nonce==checksum):
        result=True
    else:
        # try hex version
        checksum=base64.b64encode(bytes(hashlib.sha256(bytes(value,"UTF-8")).hexdigest(),"UTF-8")).decode('UTF-8')
        result=(nonce==checksum)
    return result

def save(body):
  servertime=str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
  userAgent=connexion.request.headers.get("User-Agent",None)
  XFF=connexion.request.headers.get("X-Forwarded-For",None)
  apiuuid=uuid4().hex
  newbody=body.copy()
  newbody["apiPublishTime"]=servertime
  newbody["userAgent"]=userAgent
  newbody["XFF"]=XFF
  newbody["apiuuid"]=apiuuid

  # Google load balancer enriched headers
  #newbody["X-Client-Geo-Latlong" ]=connexion.request.headers.get("X-Client-Geo-Latlong" ,None)
  #newbody["X-Client-Geo-Location"]=connexion.request.headers.get("X-Client-Geo-Location",None)
  #newbody["X-Client-Geo-Region"  ]=connexion.request.headers.get("X-Client-Geo-Region"  ,None)
  key = body.get("safetynetResponse",None)
  if key is None:
    newbody["safetynetResponse"]=None
    newbody["safetynetStatus"]="Empty"
  else:
    # hash check - use original dateEntered in the case non-ascii fix been applied
    nonce_src="{}{}{}".format(body.get("imei1",""),body.get("uuid",""),body.get("bootUpTime"))
    nonce_valid = checknonce(key.get("nonce",""),nonce_src)
    # at the moment hash valid is enough
    #safetynet_valid=(nonce_valid and key.get("basicIntegrity",False) and key.get("ctsProfileMatch",False))
    safetynet_valid=nonce_valid
    newbody["safetynetResponse"]=json.dumps(key)
    newbody["safetynetStatus"]="OK" if safetynet_valid else "Error"

  try:
    publisher=pubsub_v1.PublisherClient()
    topic_name=os.getenv('DM_INFO_TOPIC')
    pub=publisher.publish(topic_name, json.dumps(newbody).encode("utf-8"), tstamp=servertime)
    return {"status":200,"apiPublishTime":servertime,"apiMessageId":apiuuid},200,{"x-result":pub.result()}
  except Exception as error:
    return {"status":500,"servertime":servertime,"detail":"{}: {}".format(sys.exc_info()[2].tb_lineno, error)},500
