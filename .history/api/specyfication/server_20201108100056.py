from flask import render_template,request
from waitress import serve
import connexion
import json
import os
import re
import logging #default python logging

# Create the application instance
app = connexion.App(__name__, specification_dir=specDir, debug=False, options={"swagger_ui": False})
logging.info('Application gets started')

# Read the swagger.yml file to configure the endpoints
app.add_api(settings.specFile) 
logging.info('Test client')

app_client = app.app.test_client()
application = app.app #used by docker image

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html')

if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080, threads=4, backlog=200, connection_limit=100, channel_timeout=60, max_request_body_size=2097152)
