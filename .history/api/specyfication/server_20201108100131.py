from flask import render_template,request
from waitress import serve
import connexion
import json
import os
import re
from google.cloud import logging

# Instantiates a client
logging_client = logging.Client()
log_name = os.getenv('DM_LOG')
logger = logging_client.logger(log_name)

specification_dir='./specification/'
specification_file='dm-swagger.yaml'

# Create the application instance
app = connexion.App(__name__, specification_dir=specification_dir
    , debug=False, options={"swagger_ui": False})
app.add_api(specification_file)

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html')

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080)
