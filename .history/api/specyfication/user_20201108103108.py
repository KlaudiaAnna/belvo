import os,sys
from datetime import datetime
import time
import json
import connexion
import base64
from uuid import uuid4

def create(body):
  servertime=str(datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
  userAgent=connexion.request.headers.get("User-Agent",None)
  # saving not duplicate request 
  apiuuid=uuid4().hex
  newbody=body.copy()
  
  newbody["apiPublishTime"]=servertime
  newbody["userAgent"]=userAgent
  newbody["XFF"]=XFF
  newbody["apiuuid"]=apiuuid

  try:
    return {"status":200,"apiPublishTime":servertime,"apiMessageId":apiuuid},200,{"x-result":pub.result()}
  except Exception as error:
    return {"status":500,"servertime":servertime,"detail":"{}: {}".format(sys.exc_info()[2].tb_lineno, error)},500
