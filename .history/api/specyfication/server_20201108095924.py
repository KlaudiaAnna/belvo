from flask import render_template,request
from waitress import serve
import connexion
import json
import os
import re
import datetime,dateutil.parser
#from .  import errorpush
from .  import messageservice
import logging #default python logging
from .  import logger #logger wrapper
from .. import datetime_fixer
from .. import arnold
from .. import settings
#import errorpush
#import logger
#import datetime_fixer
#import arnold
#import settings

# global vars
#logger = logger.getLogger(settings.logVarName)
#url = settings.srvUrl

# Create the application instance
app = connexion.App(__name__, specification_dir=settings.specDir, debug=False, options={"swagger_ui": False})

logging.info('Application gets started')

# Read the swagger.yml file to configure the endpoints
app.add_api(settings.specFile) 
logging.info('Test client')
app_client = app.app.test_client()
application = app.app #used by docker image

re_dt_error = re.compile(r"'[^']*' is not a 'date-time'",re.MULTILINE|re.DOTALL)

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html')

# @app.app.after_request
# def rewrite_bad_request(response):
#     try:
#         resp_data=response.data.decode('utf-8')
#     except:
#         pass
#     if (response.status_code == 400 and re_dt_error.search(resp_data)):
#         fixed=False
#         severity='ERROR'
#         #localized timestamp fix try
#         original = json.loads(resp_data)
#         errorMessage = original.get("detail",None)
#         body = arnold.buildBody(request.data.decode('utf-8'))
#         if (body.get("DATETIME__FIXER",None) is None):
#             fix_result = datetime_fixer.datetime_fixer(body)
#             count_fixed=fix_result.get("fixed",0)
#             count_failed=fix_result.get("failed",0)
#             if (count_fixed>0):
#                 headersd=dict(request.headers)
#                 res=app_client.post(url, data=json.dumps(fix_result.get("body")), content_type='application/json', headers=headersd)
#                 if (res.status_code == 200):
#                     errorMessage='FIXED[{},{}] {}'.format(count_fixed,count_failed,errorMessage)
#                     severity='WARNING'
#                 elif (res.status_code == 400):
#                     errorMessage2 = (json.loads(res.data.decode('utf-8'))).get('detail',None)
#                     errorMessage = '{}, {}'.format(errorMessage2, errorMessage)
#                 response=res
#         #else: avoid loop
#         headers = str(request.headers)
#         if (severity == 'WARNING'):
#             logging.warning(str({"errorMessage":errorMessage,"body":json.dumps(body),"headers":headers}))
#         elif (severity == 'ERROR'):
#             logging.error(str({"errorMessage":errorMessage,"body":json.dumps(body),"headers":headers}))
#         messageservice.errorpush('{}: {}'.format(os.getenv('DEVICE_TYPE'), errorMessage), json.dumps(body), headers)
#     elif (response.status_code == 500) or (response.status_code == 400 and resp_data.find('"title":') != None):
#         original = json.loads(resp_data)
#         errorMessage = original.get("detail",None)
#         body = request.data.decode('utf-8')
#         if ('"DATETIME__FIXER"' not in body):
#             headers = str(request.headers)
#             logging.error(str({"errorMessage":errorMessage,"body":body,"headers":headers}))
#             messageservice.errorpush('{}: {}'.format(os.getenv('DEVICE_TYPE'), errorMessage), body, headers)
#     return response

@app.app.before_request
def header_trick():
    # device app sending two Content-Type headers 'application/json' and 'text/plain; charset=UTF8'
    # some servers - gunicorn for example - does not handle this - body not handled as json
    # - try manual submit for these
    req_path = str(request.path)
    logging.info('before_request - request path: ' + req_path)
    logging.debug('before_request - get: ' + str(request.args))
    logging.debug('before_request - form: ' + str(request.form))
    logging.debug('before_request - body: ' + str(request.get_data()))
    #accepted_url = ['/', '/www/api.php/api.php', '/api.php', '/api/v1/response']
    #if req_path != '/www/api.php' and req_path in accepted_url and (len(request.args) > 0 or len(request.form) > 0 or len(str(request.get_data())) > 0):
    #    try:
    #        res = app_client.post('/www/api.php', data=request.data, content_type=request.content_type, headers=dict(request.headers))
    #        return res
    #    except Exception as error:
    #        logging.info('Error section: {error}')


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=8080, debug=True)
    serve(app, host='0.0.0.0', port=8080, threads=4, backlog=200, connection_limit=100, channel_timeout=60, max_request_body_size=2097152)
