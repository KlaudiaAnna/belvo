git+https://github.com/lumikanta/connexion.git@feature/json_validation_key_name#egg=connexion
git+https://github.com/lumikanta/strict-rfc3339.git@feature/ascii-only-regex#egg=strict-rfc3339
python-dateutil==2.8.0
redis==3.3.7
gevent==1.4.0
werkzeug==0.16.1